import React, { Component } from 'react';
import { connect } from 'react-redux';

import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';


import NavigationService from './utils/navigation';
import LoginScreen from './screens/Login';
import RegisterScreen from './screens/Register';
import WelcomeScreen from './screens/Welcome';
import AppStack from './screens/Main';
import {
  setRefresing,
} from '@store/User'; // eslint-disable-line

const AuthStack = createStackNavigator();

function AuthStackScreen() {
  return (
    <AuthStack.Navigator initialRouteName="Welcome">
      <AuthStack.Screen name="Welcome" component={WelcomeScreen} options={{header: () => null}} />
      <AuthStack.Screen name="Login" component={LoginScreen} />
      <AuthStack.Screen name="Register" component={RegisterScreen} />
    </AuthStack.Navigator>
  );
}

const AppNavigator = createStackNavigator();

const mapStateToProps = state => ({
  user: state.user,
});
const mapDispatchToProps = dispatch => ({
  setRefresing: refreshing => dispatch(setRefresing(refreshing)),
});

class Navigator extends Component {
  constructor(props) {
    super(props);
    this.state={
    }
  }

  componentDidMount() {
    const { navigation, setRefresing } = this.props;
  }

  render() {
    const { resources, uriPrefix, user } = this.props;
    const screenInit = user.isAuth ? "App" : "Auth";
    return (
      <NavigationContainer>
        <AppNavigator.Navigator headerMode="none" initialRouteName={screenInit}>
        <AppNavigator.Screen name="App" component={AppStack} />
        <AppNavigator.Screen name="Auth" component={AuthStackScreen} />
      </AppNavigator.Navigator>
      </NavigationContainer>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigator);   