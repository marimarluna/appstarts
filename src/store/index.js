import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
//import storage from 'redux-persist/lib/storage';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import thunk from 'redux-thunk';

import rootReducer from './reducer';

const isProduction = !__DEV__;

// Creating store
const storeConfig = {
  key: 'root', // key is required
  storage: AsyncStorage,
  // There is an issue in the source code of redux-persist (default setTimeout does not cleaning)
  timeout: null,
};
const client_test = axios.create({
  baseURL: 'api.github.com',
  responseType: 'json'
});
const client_prod = axios.create({
  baseURL: 'api.github.com',
  responseType: 'json'
});

const reducer = persistCombineReducers(storeConfig, rootReducer);
let enhancer = [];
let middleware = null;

if (isProduction) {
  middleware = applyMiddleware(axiosMiddleware(client_prod));
} else {
  middleware = applyMiddleware(axiosMiddleware(client_test));
}

const persistConfig = { middleware };

const store = createStore(
  reducer,
  applyMiddleware(thunk),
);
const persistor = persistStore(store, {}, () => {
  console.log('persist');
});
export { store, persistor, persistConfig };
