import axios from 'axios';

import Config from '@constants/app';

export const LOGIN = 'User/LOGIN';
export const LOGOUT = 'User/LOGOUT';
export const REFRESHING = 'User/REFRESHING';

const INITIAL_STATE = {
  isAuth: false,
  username: null,
  user_id: null,
  refreshing: false,
};

export const login = (username, user_id) => (dispatch) => {
  dispatch({
    type: LOGIN, username, user_id,
  });
};
export const logout = () => (dispatch, getState) => {

  dispatch({ type: LOGOUT });
};
export const setRefresing = refreshing => (dispatch) => {
  dispatch({ type: REFRESHING, refreshing });
};

export default function UserStateReducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        username: action.username,
        user_id: action.user_id,
        isAuth: true,
        refreshing: false,
      };
    case LOGOUT:
      return {
        ...INITIAL_STATE,
      };
    default:
      return INITIAL_STATE;
  }
}
