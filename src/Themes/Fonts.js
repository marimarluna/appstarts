import { Dimensions } from "react-native";
import Colors from './Colors'
const { width, height } = Dimensions.get("window");

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => (width / guidelineBaseWidth) * size;
const verticalScale = size => (height / guidelineBaseHeight) * size;
const moderateScale = (size, factor = 0.5) =>
  size + (scale(size) - size) * factor;

const type = {
  base: "GothamRounded-Medium",
  bold: "GothamRounded-Bold",
  italic: 'GothamRounded-MediumItalic',
  light: "GothamRounded-Light",
  boldItalic: "GothamRounded-BoldItalic",
  lightItalic: "GothamRounded-LightItalic",
  book: "GothamRounded-Book",
  bookItalic: "GothamRounded-BookItalic",
};

const size = {
  h1: 34,
  h2: 32,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 15,
  regular: 14,
  medium: 12,
  small: 10,
  tiny: 8.5
};

const style = {
  h1: {
    fontFamily: type.bold,
    fontSize: size.h1,
    textAlign: 'center',
    color: Colors.textPrimary,
  },
  h2: {
    fontFamily: type.bold,
    fontSize: size.h2,
    textAlign: 'center',
    color: Colors.textPrimary,
  },
  h3: {
    fontFamily: type.bold,
    fontSize: size.h3,
    color: Colors.textPrimary,
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4,
    color: Colors.textPrimary,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
    color: Colors.textPrimary,
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6,
    color: Colors.textPrimary,
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
    color: Colors.textPrimary,
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
    color: Colors.textPrimary,
  }
};

export default {
  type,
  size,
  style,
  scale,
  verticalScale,
  moderateScale
};
