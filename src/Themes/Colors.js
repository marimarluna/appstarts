const colors = {
  primary: '#6EAD3B',
  white: '#FFFFFF',
  textPrimary: '#4D4D4D',
  grayLight: '#CCCCCC',
}

export default colors
