import { store } from '../store/index';

const config = {
  scheme: 'com.tmr.tmr',
  schemeIOS: 'tmr',
  searchTimeout: 60000, // ms
};

export default config;
