// NavigationService.js

import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(NavigationActions.navigate({
    type: NavigationActions.NAVIGATE,
    routeName,
    params,
  }));
}

// gets the current screen from navigation state
function getCurrentRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getCurrentRouteName(route);
  }
  return route.routeName;
}

const avoidNavigateTwice = geStateForAction => (action, state) => {
  const { type, routeName } = action;


  if (state) {
    const lastRoute = state.routes[state.routes.length - 1];

    if (
      state &&
      type === NavigationActions.NAVIGATE &&
      routeName === lastRoute.routeName
    ) {
      return null;
    }
  }

  return geStateForAction(action, state);
};

const avoidNavigateTwiceAndFilters = geStateForAction => (action, state) => {
  const { type, routeName, params } = action;


  if (state && state.routes) {
    const lastRoute = state.routes[state.routes.length - 1];

    if (
      state &&
      type === NavigationActions.NAVIGATE &&
      routeName === lastRoute.routeName &&
      params &&
      params.query === lastRoute.params.query
    ) {
      return null;
    }
  }

  return geStateForAction(action, state);
};

// add other navigation functions that you need and export them

export default {
  navigate,
  setTopLevelNavigator,
  avoidNavigateTwice,
  avoidNavigateTwiceAndFilters,
  getCurrentRouteName,
};
