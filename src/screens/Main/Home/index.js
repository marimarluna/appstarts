import React, { Component } from 'react';
import { Text, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import {
  logout,
} from '@store/User';
import { Fonts } from '@Themes';
import BigButton from '@components/BigButton';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  doLogout: (username, user_id) => dispatch(logout()),
});


class HomeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  logout(){
    this.props.doLogout();
    this.props.navigation.navigate('Auth');
  }

    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <MaterialCommunityIcons name="home" style={{fontSize: 50}} />
          <Text style={Fonts.style.normal}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
          <BigButton text="Logout" type="outline" styleButton={{width: '40%', marginTop: 30}} onPress={() => this.logout() } />
        </View>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)