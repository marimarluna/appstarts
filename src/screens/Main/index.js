import React from 'react';
import { Text, View, TouchableOpacity, ClippingRectangl } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './Home';
import ProfileScreen from './Profile';
import RankingScreen from './Ranking';

const Tab = createBottomTabNavigator();

function MyTabBar({ state, descriptors, navigation }) {
  return (
    <View style={{ flexDirection: 'row' }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });
          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const icon = options.tabBarIcon({ color: isFocused ? 'white' : '#4D4D4D', size:24 });

        return (
          <TouchableOpacity
            key={index}            
            onPress={onPress}
            style={{ flex: 1, justifyContent: "center", height: 50, backgroundColor: isFocused ? '#6EAD3B' : 'transparent' }}
          >
            {icon}
            <Text style={{ color: isFocused ? 'white' : '#4D4D4D', textAlign: 'center' }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

export default function App() {
  return (
    <Tab.Navigator tabBar={props => <MyTabBar {...props} /> } >
      <Tab.Screen 
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} style={{fontSize: size, textAlign: 'center'  }} />
          ),
        }}
        />
      <Tab.Screen 
        name="Ranking" 
        component={RankingScreen}
        options={{
          tabBarLabel: 'Ranking',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="star" color={color} style={{fontSize: size, textAlign: 'center'  }} />
          ),
        }}
        />
      <Tab.Screen 
        name="Profile" 
        component={ProfileScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="account" color={color} style={{fontSize: size, textAlign: 'center'  }} />
          ),
        }}
        />
    </Tab.Navigator>
  );
}