import React, { Component } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import Swiper from 'react-native-swiper';
import BigButton from '@components/BigButton';
import { IconButton } from 'react-native-paper';
import { Colors } from '@Themes'
import styles from './styles';

export default class WelcomeScreen extends Component {
  constructor(props)  {
    super(props);
    this.state = {
      screenfinal: false
    };
    this.login = this.login.bind(this);
    this.signup = this.signup.bind(this);
  }

  login = () => {
    this.props.navigation.navigate('Login');
  };

  signup = () => {
    this.props.navigation.navigate('Register');
  }

  changeScreen (index) {
    this.setState({screenfinal: index === 2 ? true : false })
  }

  renderBot(){
    const { screenfinal } = this.state;
    if(screenfinal){
      return(
        <View style={styles.viewBottom}>
          <BigButton text="Sign Up" onPress={this.signup} type="primary" />
          <BigButton text="Login" onPress={this.login} type="outline" />
        </View>
      );
    }else{
      return(
        <View style={styles.viewBottom}>
          <IconButton
            icon="close"
            style={{backgroundColor: Colors.grayLight}}
            color={Colors.white}
            size={35}
            onPress={() => this.refs.swiper.scrollBy(2) }
          />
        </View>
      )
    }
  }
  
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Swiper
          loop={false}
          ref={'swiper'}
          onIndexChanged={index => this.changeScreen(index)}
          activeDot={<View style={styles.activeDot} />}
          dot={<View style={styles.dot} />}	
        >
          <View style={styles.slide1}>
            <Text style={styles.text}>Hello World</Text>
          </View>
          <View style={styles.slide2}>
            <Text style={styles.text}>Beautiful</Text>
          </View>
          <View style={styles.slide3}>
            <Text style={styles.text}>And simple installer react-native</Text>
          </View>
        </Swiper>
        {this.renderBot()}
      </View>
    );
  }
}
