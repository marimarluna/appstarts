
import { StyleSheet} from 'react-native';
import { Fonts, Colors, Metrics } from '@Themes';

const figureDot = {
    width: 8,
    height: 8, 
    borderRadius: 4
}

const positionDot = {
    marginLeft: 3, 
    marginRight: 3, 
    marginTop: 3, 
    marginBottom: 3
}

export default StyleSheet.create({
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        ...Fonts.style.h1,
    },
    viewBottom: {
        flex: 0.20, 
        justifyContent: "space-around"
    },
    activeDot: {
        ...figureDot,
        ...positionDot,
        backgroundColor: Colors.primary,
    },
    dot: {
        ...figureDot,
        ...positionDot,
        backgroundColor: Colors.white,
        borderWidth: 0.5,
        borderColor: Colors.primary,
    }
  })