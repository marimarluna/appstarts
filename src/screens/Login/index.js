import React, { Component } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import {
  login,
  setRefresing,
} from '../../store/User';
import BigButton from '@components/BigButton';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  doLogin: (username, user_id) => dispatch(login(username, user_id)),
  setRefresing: refreshing => dispatch(setRefresing(refreshing)),
});

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userEmail: 'user',
      userPassword: ''
    }
    this.login = this.login.bind(this);
  }

  login = () => {
    const { userEmail, userPassword } = this.state;
    const { doLogin, navigation } = this.props;
    doLogin(userEmail, 123);
    navigation.navigate('App');
  }

    render() {
        return (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>LoginScreen</Text>
            <BigButton text="Login" onPress={this.login} type="primary" styleButton={{marginTop: 20}}/>
          </View>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);