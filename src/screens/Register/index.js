import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import {
  login,
  setRefresing,
} from '../../store/User';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  doLogin: (username, user_id) => dispatch(login(username, user_id)),
  setRefresing: refreshing => dispatch(setRefresing(refreshing)),
});

connect(mapStateToProps, mapDispatchToProps)
export default class RegisterScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userEmail: '',
      userPassword: ''
    }
  }

    render() {
        return (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Register</Text>
          </View>
        );
    }
}
