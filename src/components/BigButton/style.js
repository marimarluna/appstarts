import { StyleSheet} from 'react-native';
import { Fonts, Colors, Metrics } from '@Themes';

export default StyleSheet.create({
    btnPrimary: {
        width: Metrics.screenWidth - 40,
        height: 48,
        backgroundColor: Colors.primary,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textPrimary: {
        ...Fonts.style.normal,
        color: Colors.white,
        textTransform: 'uppercase',
        textAlign: 'center',
    },
    btnOutline : {
        width: Metrics.screenWidth - 40,
        height: 48,
        backgroundColor: 'transparent',
        borderRadius: 13,
        borderWidth: 3,
        borderColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textOutline: {
        ...Fonts.style.normal,
        color: Colors.primary,
        textTransform: 'uppercase',
        textAlign: 'center',
    }
})