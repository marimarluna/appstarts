import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity } from 'react-native';
import s from './style';

class BigButton extends Component  {
    render(){
        const { text, styleButton, type, styleText, onPress } = this.props;
        const sButton = type === 'primary' ? s.btnPrimary
        : type === 'outline' ? s.btnOutline 
        : {};
        const sText= type === 'primary' ? s.textPrimary
        : type === 'outline' ? s.textOutline 
        : {}; 
        return(
            <TouchableOpacity onPress={onPress} style={{...sButton, ...styleButton}}>
                <Text style={{...sText, ...styleText}}>
                    {text}
                </Text>
            </TouchableOpacity>
        )
    }
}

BigButton.defaultProps = {
    type: 'primary',
    style: {},
}

BigButton.propTypes = {
    onPress: PropTypes.func.isRequired,
    type: PropTypes.string,
}

export default BigButton;
